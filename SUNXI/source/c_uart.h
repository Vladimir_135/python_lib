#define SUNXI_UART0_BASE 0x05000000
#define SUNXI_UART1_BASE 0x05000400
#define SUNXI_UART2_BASE 0x05000800
#define SUNXI_UART3_BASE 0x05000c00
#define SUNXI_R_UART0_BASE 0x07080000
#define SUNXI_CCU_BASE	 0x03001000

#define PAGE_SIZE         (4*1024)
#define BLOCK_SIZE        (4*1024)
#define MAP_SIZE          (4096)
#define MAP_MASK          (MAP_SIZE - 1)

#define SETUP_OK          0
#define SETUP_DEVMEM_FAIL 1
#define SETUP_MALLOC_FAIL 2
#define SETUP_MMAP_FAIL   3

#define INPUT             0
#define OUTPUT            1

#define HIGH              1
#define LOW               0

#define PUD_OFF           0
#define PUD_DOWN          2
#define PUD_UP            1

#define OPiUARTDebug      1

int setup_uart(void);
void cleanup_uart(void);