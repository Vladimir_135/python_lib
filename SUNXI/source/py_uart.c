/*
Copyright (c) 2012-2016 Ben Croston

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in
the Software without restriction, including without limitation the rights to
use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
of the Software, and to permit persons to whom the Software is furnished to do
so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/

#include "Python.h"
#include "c_gpio.h"
#include "event_gpio.h"
#include "py_pwm.h"
#include "boards.h"
#include "common.h"
#include "constants.h"

static int uart_warnings = 1;

struct py_callback
{
  unsigned int uart;
  PyObject *py_cb;
  struct py_callback *next;
};
static struct py_callback *py_callbacks = NULL;

static const char moduledocstring[] = "UART functionality for OrangePi boards using Python";

static int mmap_uart_mem(void)
{
  int result;

  if (module_setup)
    return 0;

  result = setup_uart();
  if (result == SETUP_DEVMEM_FAIL)
  {
    PyErr_SetString(PyExc_RuntimeError, "No access to /dev/mem.  Try running as root!");
    return 1;
  } else if (result == SETUP_MALLOC_FAIL) {
    PyErr_NoMemory();
    return 2;
  } else if (result == SETUP_MMAP_FAIL) {
    PyErr_SetString(PyExc_RuntimeError, "Mmap of GPIO registers failed");
    return 3;
  } else { // result == SETUP_OK
    module_setup = 1;
		//printf("SETUP_OK!\n");
    return 0;
  }
}

// python function py_uart_setting
static PyObject *py_uart_setting(PyObject *self, PyObject *args)
{
  if (mmap_uart_mem())
    return NULL;

  Py_RETURN_NONE;
}

// python function cleanup(channel=None)
static PyObject *py_cleanup_uart(PyObject *self, PyObject *args)
{
	

  Py_RETURN_NONE;
}

PyMethodDef opi_uart_methods[] = {
  {"setting", py_uart_setting, METH_VARARGS | METH_KEYWORDS, "Setting OrangePi board model to use."},
  {"cleanup", (PyCFunction)py_cleanup_uart, METH_VARARGS | METH_KEYWORDS, "Clean up by resetting all GPIO channels that have been used by this program to INPUT with no pullup/pulldown and no event detection\n[channel] - individual channel to clean up.  Default - clean every channel that has been used."},
  {NULL, NULL, 0, NULL}
};

#if PY_MAJOR_VERSION > 2
static struct PyModuleDef opiuartmodule = {
   PyModuleDef_HEAD_INIT,
   "OPi.UART",       // name of module
   moduledocstring,  // module documentation, may be NULL
   -1,               // size of per-interpreter state of the module, or -1 if the module keeps state in global variables.
   opi_uart_methods
};
#endif

#if PY_MAJOR_VERSION > 2
PyMODINIT_FUNC PyInit_UART(void)
#else
PyMODINIT_FUNC initUART(void)
#endif
{
   int i;
   PyObject *module = NULL;

#if PY_MAJOR_VERSION > 2
   if ((module = PyModule_Create(&opiuartmodule)) == NULL)
      return NULL;
#else
   if ((module = Py_InitModule3("OPi.UART", opi_uart_methods, moduledocstring)) == NULL)
      return;
#endif

  //define_constants(module);

  if (!PyEval_ThreadsInitialized())
    PyEval_InitThreads();

  // register exit functions - last declared is called first
  //if (Py_AtExit(cleanup) != 0)
  //{
  //  setup_error = 1;
  //  cleanup_uart();
#if PY_MAJOR_VERSION > 2
  //  return NULL;
#else
   // return;
#endif
  //}

  //if (Py_AtExit(event_cleanup_all) != 0)
  //{
  //  setup_error = 1;
  //  cleanup_uart();
#if PY_MAJOR_VERSION > 2
  //  return NULL;
#else
  //  return;
#endif
  //}

#if PY_MAJOR_VERSION > 2
 return module;
#else
 return;
#endif
}
