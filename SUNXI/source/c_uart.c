#include "c_uart.h"
#include <stdint.h>
#include <stdlib.h>
#include <stdio.h>
#include <fcntl.h>
#include <sys/mman.h>
#include "c_gpio.h"
#include <unistd.h>

static volatile uint32_t *uart_map;
static volatile uint32_t *ccu_map;

uint32_t readl_uart(uint32_t* map, uint32_t addr)
{
	volatile uint32_t val = 0;
	uint32_t mmap_base = (addr & ~(uint32_t)MAP_MASK);
	uint32_t mmap_seek = (addr & (uint32_t)MAP_MASK);
	uint32_t* pointer = (uint32_t*)&(((uint8_t*)map)[mmap_seek]);
	if(OPiUARTDebug)
		printf("readl: addr=0x%x total = 0x%x\n",addr,pointer);
	val = (uint32_t)*pointer;
	if(OPiUARTDebug)
		printf("mmap_base = 0x%x\t mmap_seek = 0x%x\t gpio_map = 0x%x\t total = 0x%x val = %x\n",mmap_base,mmap_seek,uart_map,pointer,*((uint32_t*)pointer));

	return val;
}

void writel_uart(uint32_t* map, uint32_t addr, uint32_t val)
{
  uint32_t mmap_base = (addr & ~(uint32_t)MAP_MASK);
  uint32_t mmap_seek = (addr & (uint32_t)MAP_MASK);
  uint32_t* pointer = (uint32_t*)&(((uint8_t*)map)[mmap_seek]);
  *pointer = val;
}

void uart_gpio_init(void)
{ 
	setup();
	setup_gpio(120,0,0);
	setup_gpio(119,0,0);
}

int setup_uart(void)
{
	int mem_sfd;
	uint32_t PageSIZE = sysconf(_SC_PAGE_SIZE);

	uint32_t UART_BGR_REG = 0;
	uint32_t UART_HALT = 0;
	uint32_t UART_LCR = 0;
	uint32_t UART_DLL = 0; 

	if ((mem_sfd = open("/dev/mem", O_RDWR|O_SYNC) ) < 0)
	{
		return SETUP_DEVMEM_FAIL;
	}

	uart_map = (uint32_t *)mmap( NULL, BLOCK_SIZE, PROT_READ|PROT_WRITE, MAP_SHARED, mem_sfd, (uint32_t*)SUNXI_UART0_BASE);
	ccu_map = (uint32_t *)mmap( NULL, BLOCK_SIZE, PROT_READ|PROT_WRITE, MAP_SHARED, mem_sfd, (uint32_t*)SUNXI_CCU_BASE);

	if(OPiUARTDebug)
		printf("ccu_map = 0x%x uart_map = 0x%x PageSIZE = %d\n",ccu_map,uart_map,PageSIZE);

	if ((uint32_t)uart_map < 0 || (uint32_t)uart_map < 0)
		return SETUP_MMAP_FAIL;

	//Enable UART Clock
	if(OPiUARTDebug)
		printf("----------UART_SETTING--------\nEnable UART Clock\n");
	UART_BGR_REG = readl_uart((uint32_t*)ccu_map,(uint32_t)0x90c);
	UART_BGR_REG |= 0x80008;
	writel_uart((uint32_t*)ccu_map,(uint32_t)0x90c,UART_BGR_REG);
	UART_BGR_REG = readl_uart((uint32_t*)ccu_map,(uint32_t)0x90c);
	
	//Configure UART PIN
	if(OPiUARTDebug)
		printf("----------UART_SETTING--------\nConfigure UART PIN\n");
	uart_gpio_init();

	//Enable CHCFG_AT_BUSY
	if(OPiUARTDebug)
		printf("----------UART_SETTING--------\nEnable CHCFG_AT_BUSY\n");
	UART_HALT = readl_uart((uint32_t*)uart_map,(uint32_t)0xC00 + (uint32_t)0xA4);
	UART_HALT |= (uint32_t)0x02;
	writel_uart((uint32_t*)uart_map,(uint32_t)0xC00 + (uint32_t)0xA4,UART_HALT);
	UART_HALT = readl_uart((uint32_t*)uart_map,(uint32_t)0xC00 + (uint32_t)0xA4);

	//Write 1 DLAB
	if(OPiUARTDebug)
		printf("----------UART_SETTING--------\nWrite 1 DLAB\n");
	UART_LCR = readl_uart((uint32_t*)uart_map,(uint32_t)0xC00 + (uint32_t)0x0C);
	UART_LCR |= (uint32_t)(1<<7);
	writel_uart((uint32_t*)uart_map,(uint32_t)0xC00 + (uint32_t)0x0C,UART_LCR);
	UART_LCR = readl_uart((uint32_t*)uart_map,(uint32_t)0xC00 + (uint32_t)0x0C);

	//Write DLL
	if(OPiUARTDebug)
		printf("----------UART_SETTING--------\nWrite DLL\n");
	UART_DLL = 1;
	writel_uart((uint32_t*)uart_map,(uint32_t)0xC00,UART_DLL);
	UART_DLL = readl_uart((uint32_t*)uart_map,(uint32_t)0xC00);

	//Write 0 DLAB
	if(OPiUARTDebug)
		printf("----------UART_SETTING--------\nWrite 0 DLAB\n");
	UART_LCR = readl_uart((uint32_t*)uart_map,(uint32_t)0xC00 + (uint32_t)0x0C);
	UART_LCR &= ~(uint32_t)(1<<7);
	writel_uart((uint32_t*)uart_map,(uint32_t)0xC00 + (uint32_t)0x0C,UART_LCR);
	UART_LCR = readl_uart((uint32_t*)uart_map,(uint32_t)0xC00 + (uint32_t)0x0C);

	//Write LCR
	if(OPiUARTDebug)
		printf("----------UART_SETTING--------\nWrite LCR\n");
	UART_LCR = readl_uart((uint32_t*)uart_map,(uint32_t)0xC00 + (uint32_t)0x0C);
	UART_LCR &= ~0x7f;
	UART_LCR |= (uint32_t)((2<<0)|(0<<2)|(0<<3)|(1<<4));
	writel_uart((uint32_t*)uart_map,(uint32_t)0xC00 + (uint32_t)0x0C,UART_LCR);
	UART_LCR = readl_uart((uint32_t*)uart_map,(uint32_t)0xC00 + (uint32_t)0x0C);

	

	return SETUP_OK;
}

void cleanup_uart(void)
{
    munmap((caddr_t)uart_map, BLOCK_SIZE);
    munmap((caddr_t)ccu_map, BLOCK_SIZE);
}